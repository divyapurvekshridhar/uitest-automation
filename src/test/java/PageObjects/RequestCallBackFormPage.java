package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RequestCallBackFormPage {
    public WebDriver driver;

    //Constructor to initialise the driver
    public RequestCallBackFormPage(WebDriver driver) {
        this.driver = driver;
    }

    public void setExistingCustomer(String value) {
        driver.findElement(By.xpath("//span[text()='" + value + "']")).click();
    }

    public void setFirstName(String firstName) {
        driver.findElement(By.id("field-page-Page1-aboutYou-firstName")).sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        driver.findElement(By.id("field-page-Page1-aboutYou-lastName")).sendKeys(lastName);
    }

    public void selectState(String state) {
        driver.findElement(By.xpath("//div[text()='Select state']")).click();
        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='" + state + "']")));
        driver.findElement(By.xpath("//*[text()='" + state + "']")).click();
        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='" + state + "']")));
    }

    public void setEmail(String email) {
        driver.findElement(By.id("field-page-Page1-aboutYou-email")).sendKeys(email);
    }

    public void setPhoneNumber(String phoneNumber) {
        driver.findElement(By.id("field-page-Page1-aboutYou-phoneNumber")).sendKeys(phoneNumber);
    }

    public void clickSubmitButton() {
        driver.findElement(By.xpath("//span[text()='Submit']")).click();
    }

    public boolean verifySuccessMessage(String firstName) {
        if (driver.findElement(By.xpath("//h3[text()='Thank you, " + firstName + "']")).isDisplayed() && driver.findElement(By.xpath("//h1[text()=\"WE'VE RECEIVED YOUR REQUEST\"]")).isDisplayed()) {
            return true;
        }
        return false;
    }
}