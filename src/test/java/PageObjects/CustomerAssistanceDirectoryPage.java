package PageObjects;

import java.util.ArrayList;

import org.openqa.selenium.*;

public class CustomerAssistanceDirectoryPage {
    public WebDriver driver;

    //Constructor to initialise the driver
    public CustomerAssistanceDirectoryPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickNextButton() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement nextButton = (WebElement) js.executeScript("return document.querySelector(\"#contact-form-shadow-root\").shadowRoot.querySelector(\"#main-container > div > div.sc-ifAKCX.Col__StyledCol-o7bhp7-0.ibULtI > section > div.sc-bdVaJa.iAQrVS > button > div > span\")");
        js.executeScript("arguments[0].click();", nextButton);
        Thread.sleep(2000);
    }

    public void clickNewHomeLoans() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement newHomeLoansButton = (WebElement) js.executeScript("return document.querySelector(\"#contact-form-shadow-root\").shadowRoot.querySelector(\"#myRadioButton-0 > label > span\")");
        js.executeScript("arguments[0].click();", newHomeLoansButton);
    }

    public void switchToNewTab() throws InterruptedException {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        Thread.sleep(1000);
    }
}
