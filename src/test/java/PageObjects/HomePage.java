package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    public WebDriver driver;

    // Constructor to initialise the driver
    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickHomeLoans() throws InterruptedException {
        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/personal/home-loans']/div[@class='link-icon']")));
        driver.findElement(By.xpath("//a[@href='/personal/home-loans']/div[@class='link-icon']")).click();
    }
}
