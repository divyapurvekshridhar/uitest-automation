package PageObjects;

import org.openqa.selenium.*;

public class HomeLoanPage {

    public WebDriver driver;

    //Constructor to initialise the driver
    public HomeLoanPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickEnquireAboutaNewLoan() throws InterruptedException {
        driver.findElement(By.xpath("//a/p[text()='Enquire about a new loan']")).click();
    }
}
