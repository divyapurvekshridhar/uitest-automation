package StepDefinitions;

import PageObjects.CustomerAssistanceDirectoryPage;
import PageObjects.HomeLoanPage;
import PageObjects.HomePage;
import PageObjects.RequestCallBackFormPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NABHomeLoanSteps {

    public static WebDriver driver;
    HomePage homePage;
    HomeLoanPage homeLoanPage;
    CustomerAssistanceDirectoryPage customerAssistanceDirectoryPage;
    RequestCallBackFormPage requestCallBackFormPage;

    public NABHomeLoanSteps() {
        driver = Hooks.driver;
    }

    @Given("^I navigate to the NAB homepage$")
    public void i_navigate_to_the_homepage() throws IOException {
        InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/config/config.properties");
        Properties prop = new Properties();
        // load a properties file
        prop.load(input);

        // get the property value and print it out
        String NABHomePageUrl = prop.getProperty("NABHomePageUrl");
        driver.get(NABHomePageUrl);
        Assert.assertTrue("NAB Personal Banking - insurance, loans, accounts, credit cards - NAB".equalsIgnoreCase(driver.getTitle()));
        homePage = new HomePage(driver);
    }

    @When("^I click on Home Loans$")
    public void iClickOnHomeLoans() throws InterruptedException {
        homePage.clickHomeLoans();
    }

    @When("^I enquire about new Home Loan$")
    public void iEnquireAboutNewHomeLoan() throws InterruptedException {

        homeLoanPage = new HomeLoanPage(driver);
        new WebDriverWait(driver, 60).until(ExpectedConditions.titleIs("Home loans - View our flexible home loan options and calculators - NAB"));

        homeLoanPage.clickEnquireAboutaNewLoan();
        new WebDriverWait(driver, 60).until(ExpectedConditions.titleIs("Customer assistance directory | We’re here to help - NAB"));

        customerAssistanceDirectoryPage = new CustomerAssistanceDirectoryPage(driver);
        customerAssistanceDirectoryPage.clickNewHomeLoans();
        customerAssistanceDirectoryPage.clickNextButton();
        customerAssistanceDirectoryPage.switchToNewTab();
        new WebDriverWait(driver, 60).until(ExpectedConditions.titleIs("Consumer Call Centre Request Callback Form - NAB"));
    }

    @When("^I fill up the call back form with firstName \"([^\"]*)\",lastName \"([^\"]*)\", state \"([^\"]*)\",email \"([^\"]*)\",phoneNumber \"([^\"]*)\",existing Customer \"([^\"]*)\" and submit$")
    public void iFillUpTheCallBackFormWithFirstNameLastNameStateEmailPhoneNumberExistingCustomerAndSubmit(String firstName, String lastName, String state, String email, String phoneNumber, String existingCustomer) throws InterruptedException {

        requestCallBackFormPage = new RequestCallBackFormPage(driver);
        requestCallBackFormPage.setExistingCustomer(existingCustomer);
        requestCallBackFormPage.setFirstName(firstName);
        requestCallBackFormPage.setLastName(lastName);
        requestCallBackFormPage.setPhoneNumber(phoneNumber);
        requestCallBackFormPage.setEmail(email);
        requestCallBackFormPage.selectState(state);
        requestCallBackFormPage.clickSubmitButton();
        Thread.sleep(2000);
    }

    @Then("^I should be able to see request received success message for \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeRequestReceivedSuccessMessageFor(String firstName) throws Throwable {
        Assert.assertTrue(requestCallBackFormPage.verifySuccessMessage(firstName));
    }
}