package StepDefinitions;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    public static WebDriver driver;

    @Before
    public void openBrowser() throws IOException {

        createWebDriver();

        //Delete all cookies at the start of each scenario to avoid shared state between tests
        driver.manage().deleteAllCookies();

        driver.manage().window().maximize();
    }

    @After

    //Close the Browser
    public void closeBrowser() {
        driver.quit();
    }

    public void createWebDriver() {

        String webdriver = System.getProperty("Browser", "chrome");
        System.out.println("Launching test on browser " + webdriver);

        //To run tests on chrome browser
        if (webdriver.equalsIgnoreCase(("chrome"))) {
            String driverPath = System.getProperty("user.dir") + "//drivers//chromedriver";
            System.setProperty("webdriver.chrome.driver", driverPath);
            driver = new ChromeDriver();
        }

        //To run tests on firefox Browser
        else if (webdriver.equalsIgnoreCase(("firefox"))) {
            String driverPath = System.getProperty("user.dir") + "//drivers//geckodriver";
            System.setProperty("webdriver.gecko.driver", driverPath);
            driver = new FirefoxDriver();
        }

        //To run tests on Headless Browser
        else if (webdriver.equalsIgnoreCase(("headless"))) {

            String driverPath = System.getProperty("user.dir") + "//drivers//chromedriver";
            System.setProperty("webdriver.chrome.driver", driverPath);

            ChromeOptions options = new ChromeOptions();
            options.addArguments("--window-size=1920,1080");
            options.addArguments("--start-maximized");
            options.addArguments("--headless");
            driver = new ChromeDriver(options);
        } else {
            throw new RuntimeException("Unsupported webdriver: " + webdriver);
        }
    }
}
