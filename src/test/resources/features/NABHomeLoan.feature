Feature: Enquire NAB HomeLoan

  As a customer
  I want to enquire about NAB Home Loan
  so that I can apply for it

  @smoke @regression
  Scenario Outline: NAB Home Loan Enquiry
    Given I navigate to the NAB homepage
    When I click on Home Loans
    When I enquire about new Home Loan
    When I fill up the call back form with firstName "<FirstName>",lastName "<LastName>", state "<State>",email "<Email>",phoneNumber "<PhoneNumber>",existing Customer "<ExistingCustomer>" and submit
    Then I should be able to see request received success message for "<FirstName>"
    Examples:
      | FirstName | LastName | State | Email               | PhoneNumber | ExistingCustomer |
      | Kia       | Wykes    | SA    | kia.wykes@gmail.com | 0456787989  | No               |


