# UI Test Framework
=============

The Tech Stack used is as below
1. Java
2. Selenium WebDriver
3. Cucumber for BDD
4. JUnit

We use Page Object Model which is a design pattern which has become popular in test automation for enhancing test maintenance and
reducing code duplication.
A page object is an object-oriented class that serves as an interface to a page.
The tests then use the methods of this page object class whenever they need to interact with the UI of that page,
the benefit is that if the UI changes for the page, the tests themselves don’t need to be changed,
only the code within the page object needs to change.
Subsequently all changes to support that new UI are located in one place.


Pre-requisites to be set up on the machine
================================================
1. Java
2. Maven
3. Chrome Browser (version 84)
4. Firefox Browser (version 79)
5. IDE like Eclipse/IntelliJ


How to execute the Test
====================================================
1. Download/Clone the repo on to the local machine
2. Ensure Java and Maven have been installed on the system
3. The driver files in this repository are compatible for MAC on  Chrome Browser (version 84) and
    Firefox Browser (version 79). For users using different OS or different versions of Chrome Browser or Firefox Browser
    download the appropriate driver files and place in the drivers folder and set the appropriate driverPath in Hooks (/src/test/java/StepDefinitions/Hooks.java)
4. Go into the repo folder and execute the command  'mvn clean install'
5. Execute the Maven tests using the command 'mvn test'
6. The browser instance should come up and execute the tests. Default execution happens on Chrome browser
7. We can execute the test in Headless mode using command
    mvn test  -DBrowser="headless"
8. Report will be available in the target folder/ cucumber-reports /index.html
9. We can also use editor IDE like Eclipse/IntelliJ where we can import the project as a Maven project and run tests


Execute test scenarios on the command line
======================================================
1. On Different browsers 

    a. Chrome browser

        mvn test  -DBrowser="chrome"
  
    b. Headless Mode
  
        mvn test  -DBrowser="headless"
        
2. We can execute a subset of tests using tags

   a. To run smoke tests(Scenarios with tag @smoke)

         mvn test  -Dcucumber.options="--tags @smoke"

   b. To run regression tests(Scenarios with tag @regression)

        mvn test  -Dcucumber.options="--tags @regression"


